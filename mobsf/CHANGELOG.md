# [2.1.0](https://gitlab.com/to-be-continuous/mobsf/compare/2.0.2...2.1.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([1e9809f](https://gitlab.com/to-be-continuous/mobsf/commit/1e9809f8172bbb437a39f8f8c3212056345fac7f))

## [2.0.2](https://gitlab.com/to-be-continuous/mobsf/compare/2.0.1...2.0.2) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([4862533](https://gitlab.com/to-be-continuous/mobsf/commit/48625338551506c33c3e72b49152527d2a2a58af))

## [2.0.1](https://gitlab.com/to-be-continuous/mobsf/compare/2.0.0...2.0.1) (2022-10-07)


### Bug Fixes

* mkdir reports with full rights ([0e8eb5b](https://gitlab.com/to-be-continuous/mobsf/commit/0e8eb5bf6bcd516f8e2532e4845801d622e87b5a))

# [2.0.0](https://gitlab.com/to-be-continuous/mobsf/compare/1.2.0...2.0.0) (2022-08-05)


### Features

* adaptive pipeline ([5e69c0b](https://gitlab.com/to-be-continuous/mobsf/commit/5e69c0b3bd0d915b6df1ccda232cdaad673c2de0))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [1.2.0](https://gitlab.com/to-be-continuous/mobsf/compare/1.1.0...1.2.0) (2022-05-10)


### Features

* configurable tracking image ([4648921](https://gitlab.com/to-be-continuous/mobsf/commit/4648921cd34e4aac8237e370132a1fc9ed731fbd))

# [1.1.0](https://gitlab.com/to-be-continuous/mobsf/compare/1.0.1...1.1.0) (2022-02-16)


### Features

* add epheremal instance scan ([7240606](https://gitlab.com/to-be-continuous/mobsf/commit/7240606dca52fb5a91fb28634f26f432681b79b6))

## [1.0.1](https://gitlab.com/to-be-continuous/mobsf/compare/1.0.0...1.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([9c5f303](https://gitlab.com/to-be-continuous/mobsf/commit/9c5f30318cb1a6c00547c8a5282e4813d65d54ab))

# 1.0.0 (2021-09-28)


### Features

* initial template version ([d8ab177](https://gitlab.com/to-be-continuous/mobsf/commit/d8ab177ce63084f2a6bad8816e6e0d06d794d64b))
