# GitLab CI template for MobSF

This project implements a GitLab CI/CD template to perform pen-testing, malware analysis and 
security assessment for mobile applications with [Mobile Security Framework](https://github.com/MobSF/Mobile-Security-Framework-MobSF) (MobSF).

## Usage

In order to include this template in your project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'to-be-continuous/mobsf'
    ref: '2.1.0'
    file: '/templates/gitlab-ci-mobsf.yml'
```

## Global configuration

The MobSF template uses some global configuration used throughout all jobs.

| Name                   | description                                                | default value                                  |
| ---------------------- | ---------------------------------------------------------- | ---------------------------------------------- |
| `MOBSF_CLIENT_IMAGE`   | The Docker image used to send requests to the MobSF server | `registry.hub.docker.com/dwdraju/alpine-curl-jq` |
| `MOBSF_APP_FILE`       | Application package file (APK or IPA)                      | _none_                                         |
| `MOBSF_SERVER_URL`     | URL of MobSF server                                        | _none_ (runs the scan on a local server)       |
| :lock: `MOBSF_API_KEY` | API key of the MobSF server                                | _none_ (runs the scan on a local server)       |

## Jobs

Only one of the `mobsf-app-scan` and `mobsf-app-scan-service` jobs is launched depending on whenever the `MOBSF_CLIENT_IMAGE` and `MOBSF_API_KEY` are set.

### `mobsf-on-server` job

It uploads the packaged mobile application (APK or IPA) to the MobSF server described by variables, requests a scan and gets the report.

It is bound to the `package-test` stage.


### `mobsf-local` job

It runs a scan on a local MobSF server using the [official Docker image](https://hub.docker.com/r/opensecurity/mobile-security-framework-mobsf/).

It is bound to the `package-test` stage.


### Secrets management

Here are some advices about your **secrets** (variables marked with a :lock:):

1. Manage them as [project or group CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project):
    * [**masked**](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable) to prevent them from being inadvertently
      displayed in your job logs,
    * [**protected**](https://docs.gitlab.com/ee/ci/variables/#protected-cicd-variables) if you want to secure some secrets
      you don't want everyone in the project to have access to (for instance production secrets).
2. In case a secret contains [characters that prevent it from being masked](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable), 
  simply define its value as the [Base64](https://en.wikipedia.org/wiki/Base64) encoded value prefixed with `@b64@`:
  it will then be possible to mask it and the template will automatically decode it prior to using it.
3. Don't forget to escape special characters (ex: `$` -> `$$`).
